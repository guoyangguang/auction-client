//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery, place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.
require.config({
    baseUrl: 'js',
    paths: {
        'coffee-script': "lib/coffee-script",
        'cs': "lib/cs",
        'text': "lib/text",
        'jquery': "lib/jquery",
        'underscore': "lib/underscore",
        'backbone': "lib/backbone",
        'cookies': "lib/js.cookie",
        'compositeview': 'lib/composite.view',
        'flashmessage': 'lib/flash_message',
        'utils': 'lib/utils'
    },
    shim: {
        'underscore': {exports: "_"},
        'backbone': {deps: ["underscore", "jquery"], exports: "Backbone"},
        'cookies': {exports: "Cookies"}
    }
});
