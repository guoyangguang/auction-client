define [], ()->

    class Utils

        @ifFetchMore: (collection, view, afterTag=null)->
            # consider the collection number on server is zero, less than 15,
            # equal to 15, and greater than 15.
            moreLinkClassName = view.id + '-fetch-more'
            if collection.length == collection.currentLen 
                view.$('.' + moreLinkClassName).addClass('hide')
            else
                if view.$('.' + moreLinkClassName).length == 0 and collection.length >= 15
                    if afterTag
                        view.$(afterTag).after(
                            "<a class='link #{moreLinkClassName}' href='#!'>更多...</a>"
                        )
                    else
                        view.$el.append(
                            "<a class='link #{moreLinkClassName}' href='#!'>更多...</a>"
                        ) 
                if collection.length >= 15
                    collection.page++
                    collection.currentLen = collection.length
                    
        @calculate_position: (positions, number, height)->
            # :param positions: object containing position for every view 
            # :param number: number *th
            # :param height:, the number *th view height
            if number <= 2 
                position = positions[String(number)]
                position.bottom += height
            else
                referPos = positions[String(number - 2)]
                positions[String(number)] = {}
                position = positions[String(number)]
                position.left = referPos.left
                position.top = referPos.bottom
                position.bottom = position.top + height
            return [position.left, position.top]
