define ['jquery', 'underscore'], ($, _) ->

    class FlashMessage 
        
        constructor: ->
            @flashMessage = $("#flash-message")
            _.bindAll(@, 'reset')

        remote: (response)-> 
            status = response.status
            responseText = JSON.parse(response.responseText)
            switch status
                when 401
                    @prependMsg(responseText.msg)
                    @flashMessage.animate({height: 28})
                    window.setTimeout(@reset, 3000)
                when 403
                    @prependMsg(responseText.msg)
                    @flashMessage.animate({height: 28})
                    window.setTimeout(@reset, 3000)
                when 404
                    @prependMsg(responseText.msg)
                    @flashMessage.animate({height: 28})
                    window.setTimeout(@reset, 3000)
                when 400
                    msg = responseText.msg
                    if typeof(msg) == 'string'
                        @prependMsg(responseText.msg)
                        @flashMessage.animate({height: 28})
                        window.setTimeout(@reset, 3000)
                    else
                        height = 0
                        for message in msg 
                            @prependMsg(message)
                            height = height + 28
                        @flashMessage.animate({height: height})
                        window.setTimeout(@reset, 3000)

        local: (msg)-> 
            @prependMsg(msg)
            @flashMessage.animate({height: 28})
            window.setTimeout(@reset, 3000)
  
        prependMsg: (msg)->
            htmlWithData = _.template(
                "<li style='margin: 5px;'><%= message %></li>"
            )({"message": msg})
            @flashMessage.prepend(htmlWithData)
       
        reset: ->
            @flashMessage.animate({height: 0})
            @flashMessage.empty()
