define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'text!app/template/captcha/captcha.html'
], (CompositeView, _, FlashMessage, captchaHtml)->


    class CaptchaView extends CompositeView

        tagName: 'div'
        className: 'captcha'

        initialize: (options)->
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@model, 'change', @changeCallback)
            @flashMessage = new FlashMessage()
 
        events: {
            'click .captcha-img': 'fetchCaptcha'
        }

        fetchCaptcha: ->
            @model.fetch(
                {wait: true, success: @successFetch, error: @errorFetch}
            )

        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMessage.remote(response)

        changeCallback: ->
            @$el.html(
                _.template(captchaHtml)(@model.attributes)
            )
