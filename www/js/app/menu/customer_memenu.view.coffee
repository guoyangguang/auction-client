define [
  'underscore',
  'cs!compositeview',
  'cookies',
  'cs!app/profile/profile',
  'cs!app/profile/profile.view',
  'text!app/template/menu/customer_memenu.html'
], (
    _, CompositeView, Cookies, Profile, ProfileView, customerMeMenuHtml
) ->

  
    class CustomerMeMenuView extends CompositeView 
    
        tagName: 'nav'
        className: 'show-inline'
        id: 'customer-memenu' 

        initialize: (options)->
            template = _.template(customerMeMenuHtml)(
                {'thumb_file': Cookies.get('profile_avatar_url')}
            )
            @$el.html(template)
            @isMenuShown = false 
 
        events: {
            'click .customer-memenu-profile-avatar': 'toggleMenu',
            'click #customer-memenu-profile': 'navProfile',
            'click #customer-memenu-signout': 'navSignout'
        }

        toggleMenu: ->
            if @isMenuShown
                @$('.customer-memenu-menu').removeClass('show').addClass('hide')
                @isMenuShown = false
            else
                @$('.customer-memenu-menu').removeClass('hide').addClass('show')
                @isMenuShown = true 

        navProfile: ->
            if window.App.customerRouter.ifSignin()
                @toggleMenu()
                profile = new Profile()
                profileView = new ProfileView({model: profile})
                profileView.fetch()
                @appendChild(profileView)
            else
                @leave()

        navSignout: ->
            if Cookies.get('logged_in')
                @toggleMenu()
                window.App.customerRouter.navigate('signout', {trigger: true})
            else
                @leave()
