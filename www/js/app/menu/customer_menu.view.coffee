define [
  'cs!compositeview',
  'cookies',
  'cs!app/menu/customer_memenu.view',
  'text!app/template/menu/customer_menu.html'
], (
    CompositeView, Cookies, CustomerMeMenuView, customerMenuHtml
) ->

  
    class CustomerMenuView extends CompositeView 
    
        tagName: 'nav'
        id: 'customer-menu' 

        initialize: (options)->
            @$el.html(customerMenuHtml)
            
        events: {
            'click #customer-menu-discover': 'navDiscover',
            'submit #customer-menu-search': 'navSearch',
            'click #customer-menu-name': 'navAbout',
            'click #customer-menu-orders': 'navOrders'
        }

        navDiscover: ->
            window.App.customerRouter.navigate('discover', {trigger: true})

        navSearch: ->
            query = @$('#customer-menu-search input').val()
            window.App.customerRouter.navigate(
                "itemsSearch?query=#{query}", {trigger: true}
            )
            @$('#customer-menu-search input').val('')

        navAbout: ->
            window.App.customerRouter.navigate('', {trigger: true})

        navOrders: ->
            window.App.customerRouter.navigate('', {trigger: true})

        appendMeMenuView: ->
            if Cookies.get('logged_in')
                customerMeMenuView = new CustomerMeMenuView()
                @customerMeMenuView.leave() if @customerMeMenuView
                @customerMeMenuView = customerMeMenuView
                @appendChild(customerMeMenuView)
