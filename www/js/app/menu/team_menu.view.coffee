define [
  'cs!compositeview',
  'text!app/template/menu/team_menu.html'
], (CompositeView, teamMenuHtml) ->

  
    class TeamMenuView extends CompositeView 
    
        tagName: 'nav'
        id: 'team-menu' 

        initialize: (options)->
            @$el.html(teamMenuHtml)
            
        events: {
            'click #team-menu-users': 'navUsers',
            'click #team-menu-roles': 'navRoles',
            'click #team-menu-artists': 'navArtists',
        }

        navUsers: ->
            window.App.teamRouter.navigate('team/users', {trigger: true})

        navRoles: ->
            window.App.teamRouter.navigate('team/roles', {trigger: true})

        navArtists: ->
            window.App.teamRouter.navigate('team/artists', {trigger: true})
