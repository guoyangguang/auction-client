define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'cs!app/comment/reply.view',
    'text!app/template/comment/item_comment.html',
    'text!app/template/comment/replied.html'
], (_, CompositeView, FlashMessage, ReplyView, itemCommentHtml, repliedHtml)->


    class ItemCommentView extends CompositeView

        tagName: 'li'
        className: 'item-comment'

        initialize: (options)->

        events: {
            'click .item-comment-reply': 'reply'
        }

        reply: ->
            if @parent.reply
                @parent.reply.leave()
            # model is shared
            replyView = new ReplyView({model: @model})
            @parent.prependChildTo(replyView, '.item-comments-create')
            @parent.reply = replyView 

        render: ->
            @$el.html(
                _.template(itemCommentHtml)(@model.attributes)
            )
            comment = @model.get('comment')
            if comment
                @$('.item-comment-body').prepend(
                    _.template(repliedHtml)(comment)
                )
            @
