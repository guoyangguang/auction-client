define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'cs!app/comment/item_comment.view',
    'text!app/template/comment/item_comments.html'
], (_, CompositeView, FlashMessage, ItemCommentView, itemCommentsHtml)->


    class ItemCommentsView extends CompositeView

        id: 'item-comments'

        initialize: (options)->
            @$el.html(itemCommentsHtml)
            _.bindAll(
                @, 'addCallback', 'successCreate', 'errorCreate'
            )
            @listenTo(@collection, 'add', @addCallback)
            @flashMsg = new FlashMessage()
            @collection.forEach(@addCallback) 
            @reply = null

        events: {
            'click .item-comments-send': 'create'
        }

        create: ->
            data = {
                body: @$('.item-comments-body').val()
            }
            if @reply
                commentId = @reply.$('.comment-id').attr('val')
                data.comment_id = commentId
            @collection.create(
                data,
                {wait: true, success: @successCreate, error: @errorCreate}
            )

        successCreate: (model, response, options)->
            if @reply
                @reply.leave()
                @reply = null
            @$('.item-comments-body').val('')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        addCallback: (comment)->
            commentView = new ItemCommentView({model: comment})
            @appendChildTo(commentView, '.item-comments')
