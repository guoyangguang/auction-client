define [
    'underscore',
    'cs!compositeview',
    'text!app/template/comment/reply.html'
], (_, CompositeView, replyHtml)->


    class ReplyView extends CompositeView

        id: 'reply'

        initialize: (options)->

        events: {
            'click .reply-close': 'close'
        }

        close: ->
            @parent.reply = null
            @leave()

        render: ->
            @$el.html(
                _.template(replyHtml)(@model.attributes)
            )
            @
