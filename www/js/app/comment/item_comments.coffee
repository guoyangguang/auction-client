define [
    'backbone',
    'cs!app/comment/item_comment'
], (Backbone, ItemComment)->


    class ItemComments extends Backbone.Collection

        initialize: (models, options)->
           @itemId = options.itemId if options.itemId

        model: ItemComment

        url: -> "/api/items/#{@itemId}/comments"
