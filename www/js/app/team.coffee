require [
    'jquery',
    'backbone',
    'cs!app/team_router'
], ($, Backbone, TeamRouter) ->

    $(document).ready ->
        window.App = {teamRouter: new TeamRouter()}
        Backbone.history.start(pushState: true) 
        $(document).ajaxStart -> $('#loading').fadeIn()
        $(document).ajaxStop -> $('#loading').fadeOut()
