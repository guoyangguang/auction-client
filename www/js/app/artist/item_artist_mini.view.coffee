define [
    'underscore',
    'cs!compositeview',
    'cs!app/artist/item_artist.view',
    'text!app/template/artist/item_artist_mini.html'
], (_, CompositeView, ItemArtistView, itemArtistMiniHtml)->

 
    class ItemArtistMiniView extends CompositeView

        className: 'item-artist-mini'
        tagName: 'li'

        initialize: (options)->
            @itemArtistView = null

        events: {
            'click': 'show'
        }

        show: ->
            if @itemArtistView
                @itemArtistView.leave()
                @itemArtistView = null
            else
                itemArtistView = new ItemArtistView({model: @model})
                @itemArtistView = itemArtistView
                @parent.appendChildTo(itemArtistView, '.item-show-header')

        render: ->
            template = _.template(itemArtistMiniHtml)
            @$el.html(template(@model.attributes))
            @
