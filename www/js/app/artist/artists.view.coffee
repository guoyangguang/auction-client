define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'cs!app/artist/artist.view',
    'cs!utils',
    'text!app/template/artist/artists.html'
], (
    CompositeView, _, FlashMessage, ArtistView, Utils, ArtistsHtml
)->

    class ArtistsView extends CompositeView

        id: 'artists'

        initialize: ->
            @$el.html(ArtistsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch', 'successCreate', 'errorCreate'
            )
            @listenTo(@collection, 'add', @addCallback)

        events: {
            'click .artists-fetch-more': 'fetch',
            'click .artists-sub': 'create'
        }

        fetch: ->
            data = {
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, '.artists')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        create: ->
            formData = new FormData(@$('form')[0])
            # options to tell jQuery not to process data
            options = {
                type: 'POST',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successCreate,
                error: @errorCreate
            }
            @collection.create(null, options)

        successCreate: (model, response, options)->

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        addCallback: (artist)->
            artistView = new ArtistView({model: artist})
            @appendChildTo(artistView, '.artists')
