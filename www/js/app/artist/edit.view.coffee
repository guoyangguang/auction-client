define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'text!app/template/artist/edit.html'
], (CompositeView, _, FlashMessage, editHtml)->

    class ArtistEditView extends CompositeView

        className: 'artist-edit'

        initialize: ->
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successUpdate', 'errorUpdate')
            template = _.template(editHtml)
            @$el.html(template(@model.attributes))

        events: {
            'click .artist-edit-btn': 'update',
            'click .artist-edit-close': 'close'
        }

        update: ->
            formData = new FormData(@$('form')[0])
            # options to tell jQuery not to process data
            options = {
                type: 'PUT',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successUpdate,
                error: @errorUpdate
            }
            @model.save(null, options)

        successUpdate: (model, response, options)->
            @leave()

        errorUpdate: (model, response, options)->
            @flashMsg.remote(response)

        close: ->
            @leave()
