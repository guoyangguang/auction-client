define [
    'cs!compositeview',
    'underscore',
    'cs!app/artist/edit.view',
    'cs!app/item/artist_items',
    'cs!app/item/artist_items.view',
    'text!app/template/artist/artist.html'
], (CompositeView, _, ArtistEditView, ArtistItems, ArtistItemsView, artistHtml)->

    class ArtistView extends CompositeView

        className: 'artist dashed-bottom'
        tagName: 'li'

        initialize: ->
            @listenTo(@model, 'change', @changeCallback)

        events: {
            'click .artist-edit': 'edit',
            'click .artist-items': 'artistItems'
        }

        edit: ->
           artistEditView = new ArtistEditView({model: @model})
           @parent.appendChild(artistEditView)

        artistItems: ->
            artistItems = new ArtistItems(null, {artist: @model})
            artistItemsView = new ArtistItemsView(
                {collection: artistItems}
            )
            artistItemsView.fetch()
            @parent.appendChild(artistItemsView)

        changeCallback: ->
            @render()

        render: ->
            template = _.template(artistHtml)
            @$el.html(template(@model.attributes))
            @
