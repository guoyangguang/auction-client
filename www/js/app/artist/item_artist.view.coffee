define [
    'underscore',
    'cs!compositeview',
    'cs!app/followship/following_mine',
    'cs!app/followship/following_mine.view',
    'text!app/template/artist/item_artist.html'
], (_, CompositeView, FollowingMine, FollowingMineView, itemArtistHtml)->

 
    class ItemArtistView extends CompositeView

        className: 'item-artist'

        render: ->
            template = _.template(itemArtistHtml)
            @$el.html(template(@model.attributes))
            followingMineView = new FollowingMineView({
                model: new FollowingMine()
            })
            @prependChild(followingMineView)
            @
