define [
    'backbone',
    'cs!app/artist/artist'
], (Backbone, Artist)->

    class Artists extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0

        url: '/api/artists'

        model: Artist
