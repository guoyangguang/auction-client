define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'text!app/template/about/about.html'
], (_, CompositeView, FlashMesssage, aboutHtml)->


    class AboutView extends CompositeView

        id: 'about'

        initialize: (options)->
            @$el.html(aboutHtml)

        events: {}
