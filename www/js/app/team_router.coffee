define [
    'backbone',
    'jquery',
    'cookies',
    'cs!flashmessage',
    'cs!app/menu/team_menu.view',
    'cs!app/user/users',
    'cs!app/user/users.view',
    'cs!app/role/roles',
    'cs!app/role/roles.view',
    'cs!app/artist/artists',
    'cs!app/artist/artists.view',
    'cs!app/followship/artist_followeds',
    'cs!app/followship/artist_followeds.view',
], (
    Backbone, $, Cookies, FlashMessage, TeamMenuView, Users, UsersView,
    Roles, RolesView, Artists, ArtistsView,
    ArtistFolloweds, ArtistFollowedsView
)->

    class TeamRouter extends Backbone.Router

        initialize: (options)->
            @teamMenuView = new TeamMenuView()
            @flashMessage = new FlashMessage()
            $('body').prepend(@teamMenuView.el)

        swap: (newView=null)->
            @currentView.leave() if @currentView && @currentView.leave
            @currentView = newView

        ifSignin: ->
            if Cookies.get('logged_in')
                true
            else
                @flashMessage.local('请登录')
                false

        routes: {
            'team/users(/)': 'users',
            'team/roles(/)': 'roles',
            'team/artists(/)': 'artists',
            'artist/followeds(/)': 'artistFolloweds'
        }

        users: ->
            if @ifSignin()
                users = new Users()
                usersView = new UsersView({collection: users})
                usersView.fetch()
                @swap(usersView)
                $('#main').append(usersView.el)

        roles: ->
            if @ifSignin()
                roles = new Roles()
                rolesView = new RolesView({collection: roles})
                rolesView.fetch()
                @swap(rolesView)
                $('#main').append(rolesView.el)

        artists: ->
            if @ifSignin()
                artists = new Artists()
                artistsView = new ArtistsView(
                    {collection: artists}
                )
                artistsView.fetch()
                @swap(artistsView)
                $('#main').append(artistsView.el)

        artistFolloweds: ->
            if @ifSignin()
                artistFolloweds = new ArtistFolloweds()
                artistFollowedsView = new ArtistFollowedsView({
                    collection: artistFolloweds
                })
                artistFollowedsView.fetch()
                @swap(artistFollowedsView)
                $('#main').append(artistFollowedsView.el)
