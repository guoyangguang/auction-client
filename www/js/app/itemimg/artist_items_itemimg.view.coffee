define [
    'cs!compositeview',
    'text!app/template/itemimg/artist_items_itemimg.html'
], (CompositeView, artistItemsItemImgHtml)->

 
    class ArtistItemsItemImgView extends CompositeView

        className: 'artist-items-itemimg'

        tagName: 'li'

        render: ->
            template = _.template(artistItemsItemImgHtml)
            @$el.html(template(@model.attributes))
            @
