define [
    'backbone',
    'cs!app/itemimg/artist_items_itemimg'
], (Backbone, ArtistItemsItemImg)->


    class ArtistItemsItemImgs extends Backbone.Collection

        initialize: (models, options)->
            @itemId = options.itemId if options.itemId

        model: ArtistItemsItemImg

        url: -> 
            "/api/artist/items/#{@itemId}/itemimgs"
