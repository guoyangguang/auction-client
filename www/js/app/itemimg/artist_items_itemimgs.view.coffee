define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage'
    'cs!app/itemimg/artist_items_itemimg.view',
    'text!app/template/itemimg/artist_items_itemimgs.html'
], (
    _, CompositeView, FlashMessage, ArtistItemsItemImgView,
    artistItemsItemImgsHtml
)->

 
    class ArtistItemsItemImgsView extends CompositeView

        id: 'artist-items-itemimgs'

        initialize: (options)->
            @$el.html(artistItemsItemImgsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successCreate', 'errorCreate', 'addCallback')
            @collection.forEach(@addCallback)
            @listenTo(@collection, 'add', @addCallback)

        events: {
            'click .artist-items-itemimgs-sub': 'create'
        }

        create: ->
            formData = new FormData(@$('form')[0])
            # options to tell jQuery not to process data
            options = {
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successCreate,
                error: @errorCreate
            }
            @collection.create(null, options)

        successCreate: (collection, response, options)->
            @flashMsg.local('已添加.')

        errorCreate: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (itemimg)->
            itemImgView = new ArtistItemsItemImgView({model: itemimg})
            @appendChildTo(itemImgView, '.artist-items-itemimgs')
