define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'text!app/template/like/like_mine.html'
], (_, CompositeView, FlashMesssage, likeMineHtml)->


    class LikeMineView extends CompositeView

        className: 'like-mine'
        tagName: 'li'

        initialize: (options)->
            @$el.html(likeMineHtml)
            @itemId = options.itemId
            @isLiked = options.isLiked
            @$('.like-count').text(options.likeCount)
            _.bindAll(
                @, 'successLike', 'errorLike', 'successUnLike', 'errorUnLike' 
            )
            @flashMsg = new FlashMesssage()

        events: {
            'click .like-mine-btn': 'likeUnlike'
        }

        likeUnlike: ->
            if @isLiked
                @unlike()
            else
                @like()

        like: ->
            @model.url = '/api/likes/mine'
            @model.save(
                {id: @itemId},
                {
                    type: 'POST',
                    wait: true,
                    success: @successLike,
                    error: @errorLike
                }
            )

        successLike: (model, response, options)->
            # TODO change like image
            @$('.like-count').text(model.get('item_liked_count'))
            @isLiked = true
            @flashMsg.local('已喜欢.')

        errorLike: (model, response, options)->
            @flashMsg.remote(response)

        unlike: ->
            @model.url = "/api/likes/mine/#{@itemId}"
            @model.save(
                null,
                {
                    type: 'PUT',
                    wait: true,
                    success: @successUnLike,
                    error: @errorUnLike
                }
            )

        successUnLike: (model, response, options)->
            # TODO change like image
            @$('.like-count').text(model.get('item_liked_count'))
            @isLiked = false 
            @flashMsg.local('已取消喜欢.')

        errorUnLike: (model, response, options)->
            @flashMsg.remote(response)
