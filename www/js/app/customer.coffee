require [
    'jquery',
    'backbone',
    'cs!app/customer_router'
], ($, Backbone, CustomerRouter) ->

    $(document).ready ->
        window.App = {customerRouter: new CustomerRouter()}
        Backbone.history.start(pushState: true) 
        $(document).ajaxStart -> $('#loading').fadeIn()
        $(document).ajaxStop -> $('#loading').fadeOut()
