define [
    'cs!compositeview',
    'cs!app/access_token/access_token',
    'cs!app/access_token/share.view',
    'text!app/template/access_token/share_btn.html'
], (CompositeView, AccessToken, ShareView, shareBtnHtml)-> 


    class ShareBtnView extends CompositeView

        className: 'share-btn' 
        tagName: 'li'

        initialize: (options)->
            @shareView = null
            @itemId = options.itemId
            @$el.html(shareBtnHtml)

        events: {
            'click .share-btn-btn': 'appendShare'
        }

        appendShare: ->
            if @shareView
                @shareView.leave()
                @shareView = null 
            else
                access_token = new AccessToken()
                shareView = new ShareView({model: access_token})
                @shareView = shareView
                @parent.appendChildTo(shareView, '.item-show-header')
