define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'text!app/template/access_token/share.html'
], (_, CompositeView, FlashMessage, shareHtml)-> 


    class ShareView extends CompositeView

        className: 'share'

        initialize: (options)->
            @$el.html(shareHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()

        events: {
            'click .share-sub': 'fetch',
            'mouseover .share-site': 'showAuth',
            'mouseleave .share-site': 'hideAuth'
        }

        close: ->
            @leave()

        fetch: ->
            sitesArr = []
            if @$('.share-site-douban:checked').length is 1 
                sitesArr.push('sites=1') 
            if @$('.share-site-sina:checked').length is 1 
                sitesArr.push('sites=2') 
            if sitesArr.length is 0
                @flashMsg.local('请勾选分享站点.')
            else
                @model.url = '/api/access_tokens/mine/pubstatus?' + sitesArr.join('&')
                shareLink = "http://127.0.0.1/items/#{@parent.itemId}" 
                data = {
                    share: @$('.share-input').val() + ' 看这里' + shareLink
                }
                @model.fetch({
                    data: data,
                    wait: true,
                    success: @successFetch, error: @errorFetch
                })

        successFetch: (model, response, options)->
            @close()

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        showAuth: (event)->
            @$(event.target).find('a').removeClass('hide').addClass('show-inline')

        hideAuth: (event)->
            @$(event.target).find('a').removeClass('show-inline').addClass('hide')
