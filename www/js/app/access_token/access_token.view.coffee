define [
    'cs!compositeview', 
    'underscore',
    'cs!flashmessage'
], (CompositeView, _, FlashMessage)->

    class AccessTokenView extends CompositeView
    
        className: 'access-token'

        initialize: (options)->
            @code = options.code
            @site = options.site 
            _.bindAll(
                @, 'successToken', 'errorToken'
            )
            @flashMessage = new FlashMessage()

        token: ()->
            @model.url = '/api/access_tokens/mine'
            @model.save(
                {
                    code: @code,
                    site: @convertSite(@site)
                }, 
                {wait: true, success: @successToken, error: @errorToken}
            )

        successToken: (model, response, options)->
            @flashMessage.local('已授权.')
            window.App.router.navigate('', {trigger: true})

        errorToken: (model, response, options)->
            @flashMessage.remote(response)

        convertSite: (site)->
            if site is 'douban'
                return '1'
            if site is 'sina'
                return '2'
