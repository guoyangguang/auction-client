define [
    'backbone',
    'jquery',
    'cookies',
    'cs!flashmessage',
    'cs!app/menu/customer_menu.view',
    'cs!app/user/auth',
    'cs!app/user/signin.view',
    'cs!app/user/signout.view',
    'cs!app/user/signup.view',
    'cs!app/user/reset_password.view',
    'cs!app/item/discover.view',
    'cs!app/item/items_search',
    'cs!app/item/items_search.view',
    'cs!app/captcha/captcha',
    'cs!app/captcha/captcha.view',
    'cs!app/access_token/access_token',
    'cs!app/access_token/access_token.view',
    'cs!app/about/about',
    'cs!app/about/about.view'
], (
    Backbone, $, Cookies, FlashMessage, CustomerMenuView, Auth,
    SigninView, SignoutView, SignupView, ResetPasswordView,
    DiscoverView, ItemsSearch, ItemsSearchView, Captcha, CaptchaView, AccessToken,
    AccessTokenView, About, AboutView
)->

    class CustomerRouter extends Backbone.Router

        initialize: (options)->
            @customerMenuView = new CustomerMenuView()
            @customerMenuView.appendMeMenuView()
            @flashMessage = new FlashMessage()
            $('body').prepend(@customerMenuView.el)

        swap: (newView=null)->
            @currentView.leave() if @currentView && @currentView.leave
            @currentView = newView

        ifSignin: ->
            if Cookies.get('logged_in')
                true
            else
                @navigate('signin', {trigger: true})
                false

        routes: {
            'signup(/)': 'signup',
            'signin(/)': 'signin',
            'signout(/)': 'signout',
            'reset_password(/)': 'resetPassword',
            'discover(/)': 'discover',
            'itemsSearch(/)': 'itemsSearch',
            '(/)': 'about',
            ':site/oauth/callback(/)': 'accessToken',
        }

        signup: ->
            if Cookies.get('logged_in')
                @flashMessage.local('请退出.')
                @navigate('', {trigger: true})
            else
                signup = new Auth()
                signupView = new SignupView(model: signup)
                @swap(signupView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                signupView.renderChildInto(captchaView, '.signup-captcha')
                $('#main').append(signupView.el)

        signin: ->
            if Cookies.get('logged_in')
                @flashMessage.local('请退出.')
                @navigate('', {trigger: true})
            else
                signin = new Auth()
                signinView = new SigninView(model: signin)
                @swap(signinView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                signinView.renderChildInto(captchaView, '.signin-captcha')
                $('#main').append(signinView.el)

        signout: ->
            if Cookies.get('logged_in')
                auth = new Auth()
                signoutView = new SignoutView(model: auth)
                signoutView.signOut()
            else
                @flashMessage.local('请登录.')
                @navigate('signin', {trigger: true})

        resetPassword: ->
            if Cookies.get('logged_in')
                @flashMessage.local('请退出.')
                @navigate('', {trigger: true})
            else
                resetPass = new Auth()
                resetPassView = new ResetPasswordView(model: resetPass)
                @swap(resetPassView)
                captcha = new Captcha()
                captchaView = new CaptchaView({model: captcha})
                captchaView.fetchCaptcha()
                resetPassView.renderChildInto(
                    captchaView, '.reset-password-captcha'
                )
                $('#main').append(resetPassView.el)

        discover: ->
            discoverView = new DiscoverView()
            discoverView.fetchAllItems()
            @swap(discoverView)
            $('#main').append(discoverView.el)

        itemsSearch: (queryParam)->
            if @ifSignin()
                itemsSearch = new ItemsSearch()
                itemsSearchView = new ItemsSearchView(
                    {collection: itemsSearch, query: queryParam.replace(/query=/, '')}
                )
                itemsSearchView.fetch()
                @swap(itemsSearchView)
                $('#main').append(itemsSearchView.el)

        about: ->
            about = new About()
            aboutView = new AboutView({model: about})
            @swap(aboutView)
            $('#main').append(aboutView.el)

        accessToken: (site, queryParam)->
            if queryParam.match(/code=/)
                code = queryParam.replace(/code=/, '')
                accessToken = new AccessToken()
                accessTokenView = new AccessTokenView(
                    {model: accessToken, site: site, code: code}
                )
                accessTokenView.token()
                $('#main').append(accessTokenView.el)
            else
                @flashMessage.local('未授权.')
