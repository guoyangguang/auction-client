define [
    'backbone'
], (Backbone)->

    class Assignment extends Backbone.Model

        urlRoot: '/api/assignments'
