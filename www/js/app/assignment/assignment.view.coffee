define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'text!app/template/assignment/assignment.html'
], (CompositeView, _, FlashMessage, assignmentHtml)->

    class AssignmentView extends CompositeView

        id: 'assignment'

        initialize: (options)->
            @$el.html(assignmentHtml)
            _.bindAll(@, 'successAssign', 'errorAssign')
            @flashMsg = new FlashMessage()

        events: {
            'click #assignment-submit': 'assign',
            'click .assignment-cancel': 'cancel'
        }

        assign: ->
            @model.save(
                {
                    'user_id': @model.get('user_id'),
                    'role_name': @$("input[name='role_name']:checked").val() 
                },
                {wait: true, success: @successAssign, error: @errorAssign}
            )

        successAssign: (model, response, options)->
            @flashMsg.local('已添加.')
            @leave()

        errorAssign: (model, response, options)->
            @flashMsg.remote(response)

        cancel: ->
            @leave()
