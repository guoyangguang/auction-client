define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'text!app/template/profile/profile_general_edit.html'
], (CompositeView, _, FlashMessage, profileGeneralEditHtml)->

    class ProfileGeneralEditView extends CompositeView

        id: 'profile-general-edit'
        className: 'no-padding-ul'
        tagName: 'ul'

        initialize: ->
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successUpdate', 'errorUpdate')
            @$el.html(
                (_.template(profileGeneralEditHtml))(@model.attributes)
            )
            if @model.get('gender') == 0
                @$("input[value='0']").attr('checked': 'checked')
            else
                @$('input[value="1"]').attr('checked': 'checked')
            job = @model.get('job')
            @$("select[name='job'] option[value='#{job}']").attr(
                'selected': 'selected'
            )

        events: {
            'click .profile-general-edit-cancel': 'close',
            'click .profile-general-edit-sub': 'updateGeneral'
        }

        close: ->
            @parent.generalEditView = null
            @leave()
            @parent.$('.profile-general').removeClass('hide').addClass('show')

        updateGeneral: ->
            @model.url = '/api/profile'
            data = {
                name: @$("input[name='name']").val(),
                gender: @$("input[name='gender']:checked").val(),
                location: @$("input[name='location']").val(),
                job: @$("select[name='job']").val(),
                about: @$("textarea[name='about']").val()
            }
            options = {
                wait: true, 
                success: @successUpdate,
                error: @errorUpdate
            }
            @model.save(data, options)

        successUpdate: (model, response, options)->
            @parent.generalEditView = null
            @leave()
            @parent.$('.profile-general').removeClass('hide').addClass('show')

        errorUpdate: (model, response, options)->
            @flashMsg.remote(response)
