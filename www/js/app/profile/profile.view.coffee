define [
    'cs!compositeview',
    'underscore',
    'cs!flashmessage',
    'cookies',
    'cs!app/profile/profile_general_edit.view'
    'cs!app/template/share/appdata',
    'text!app/template/profile/profile.html'
], (
    CompositeView, _, FlashMessage, Cookies, ProfileGeneralEditView,
    AppData, profileHtml
)->


    class ProfileView extends CompositeView

        id: 'profile'

        initialize: ->
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch', 'successUpload', 'errorUpload'
            )
            @listenTo(@model, 'change', @changeCallback)

        events: {
            'click .profile-close': 'close',
            'click .profile-general-edit': 'editGeneral',
            'change .profile-file-input': 'uploadFile'
        }

        close: ->
            @leave()

        fetch: ->
            @model.url = '/api/profile'
            @model.fetch({
                wait: true,
                success: @successFetch,
                error: @errorFetch,
            })

        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        editGeneral: ->
            if !@generalEditView
                generalEditView = new ProfileGeneralEditView(
                    {model: @model}
                )
                @generalEditView = generalEditView
                @$('.profile-general').removeClass('show').addClass('hide')
                @appendChildTo(generalEditView, '.profile-popup')

        uploadFile: ->
            @model.url = '/api/profile/file'
            formData = new FormData(@$('form')[0])
            # options to tell jQuery not to process data
            options = {
                type: 'PUT',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successUpload,
                error: @errorUpload
            }
            @model.save(null, options)

        successUpload: (model, response, options)->

        errorUpload: (model, response, options)->
            @flashMsg.remote(response)

        changeCallback: ->
            Cookies.set('profile_avatar_url', @model.get('thumb_file'))
            template = _.template(profileHtml)
            @$el.html(template(@model.attributes))
            gender = AppData.gender[@model.get('gender')]
            job = AppData.job[@model.get('job')]
            @$('.profile-general-gender').text(gender)
            @$('.profile-general-job').text(job)
            @
