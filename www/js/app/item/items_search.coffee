define [
    'backbone',
    'cs!app/item/item'
], (Backbone, Item)->


    class ItemsSearch extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @number = 0

        model: Item

        url: "/api/items/search"
