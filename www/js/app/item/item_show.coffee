define [
    'backbone'
], (Backbone)->


    class ItemShow extends Backbone.Model

        initialize: (attributes, options)->
            @itemId = options.itemId if options.itemId

        urlRoot:  -> 
            return "/api/items/#{@itemId}"
