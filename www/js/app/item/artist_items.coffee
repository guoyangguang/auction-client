define [
    'backbone',
    'cs!app/item/artist_item'
], (Backbone, ArtistItem)->


    class ArtistItems extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @artist = options.artist if options.artist

        model: ArtistItem

        url: ->
            "/api/artists/#{@artist.id}/items"
