define [
  'cs!compositeview'
  'cs!app/item/items',
  'cs!app/item/items.view',
  'cs!app/followship/my_followings',
  'cs!app/followship/my_followings.view',
  'text!app/template/item/discover.html'
], (
    CompositeView, Items, ItemsView, MyFollowings, MyFollowingsView,
    discoverHtml
)->


    class DiscoverView extends CompositeView

        id: 'discover'

        initialize: (options)->  
            @$el.html(discoverHtml)

        events: {
            'click #myfollowings': 'myFollowings'
        }

        swap: (newView=null)->
            @currentView.leave() if @currentView && @currentView.leave
            @currentView = newView

        fetchAllItems: ->
            items = new Items()
            itemsView = new ItemsView({collection: items})
            itemsView.fetch()
            @swap(itemsView)
            @prependChild(itemsView)

        myFollowings: ->
            if window.App.customerRouter.ifSignin()
                myFollowings = new MyFollowings()
                myFollowingsView = new MyFollowingsView({
                    collection: myFollowings
                })
                myFollowingsView.fetch()
                @appendChild(myFollowingsView)
