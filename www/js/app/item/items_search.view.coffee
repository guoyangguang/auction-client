define [
    'jquery',
    'underscore',
    'cs!compositeview',
    'cs!utils',
    'cs!app/item/item.view',
    'cs!flashmessage',
    'text!app/template/item/items_search.html'
], (
    $, _, CompositeView, Utils, ItemView, FlashMessage, itemsSearchHtml
)-> 


    class ItemsSearchView extends CompositeView

        id: 'items-search'

        initialize: (options)->  
            @query = options.query
            @$el.html(itemsSearchHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 165, top: 65, bottom: 65},
                2: {left: 165 + itemWidth, top: 65, bottom: 65}
            #    3: {left: 165 + itemWidth * 2, top: 65, bottom: 65},
            #     4: {left: 165 + itemWidth * 3, top: 65, bottom: 65}
            }

        events: {
            'click .items-search-fetch-more': 'fetch'
        }

        fetch: ->
            data = {
                query: @query,
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @)
            docHeight = $(document).height() + 25
            @$('.items-search-fetch-more').css({
                'position': 'absolute',
                'left': '165px',
                'top': "#{docHeight}px",
                'z-index': 1,
                'margin-bottom': '50px'
            })

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (item)->
            itemView = new ItemView({model: item})
            @appendChild(itemView)

            @collection.number++
            thumbHeight = item.get('img_meta').vdimensions.thumb[1]
            itemView.$('.item-thumb').css('height', thumbHeight)
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                itemView.$el.outerHeight(true) + 25 
            )
            itemView.$el.css({
                'position': 'absolute',
                'z-index': 1,
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'width': '235px',
                'cursor': 'pointer',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })
