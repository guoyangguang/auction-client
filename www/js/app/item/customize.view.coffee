define [
  'cs!compositeview'
  'text!app/template/item/customize.html'
], (
    CompositeView, customizeHtml
)->


    class CustomizeView extends CompositeView

        id: 'customize'

        initialize: (options)->  
            @$el.html(customizeHtml)

        events: {
            'click .close': 'close'
        }

        close: ->
            @leave()
