define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'cs!app/itemimg/artist_items_itemimgs',
    'cs!app/itemimg/artist_items_itemimgs.view',
    'text!app/template/item/artist_item_show.html'
], (
    _, CompositeView, FlashMessage, ArtistItemsItemImgs,
    ArtistItemsItemImgsView, artistItemShowHtml
)-> 


    class ArtistItemShowView extends CompositeView

        id: 'artist-item-show'

        initialize: (options)->  
            @flashMsg = new FlashMessage()
            @listenTo(@model, 'change', @changeCallback)
            _.bindAll(
                @, 'successFetch', 'errorFetch'
            )

        events: {
            'click .artist-item-show-close': 'close'
        }

        close: ->
            @leave()

        fetch: ->
            @model.fetch(
                wait: true, 
                success: @successFetch,
                error: @errorFetch
            )

        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        changeCallback: ->
            template = _.template(artistItemShowHtml)
            @$el.html(template(@model.attributes))
            itemImgs = new ArtistItemsItemImgs(
                @model.get('itemimgs'), {itemId: @model.id}
            )
            itemImgsView = new ArtistItemsItemImgsView({collection: itemImgs})
            @appendChildTo(itemImgsView, '.artist-item-show-popup')
