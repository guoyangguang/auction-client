define [
    'jquery',
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'cs!utils',
    'cs!app/item/item.view',
    'cs!app/item/customize.view',
    'text!app/template/item/following_items.html',
], (
    $, _, CompositeView, FlashMessage, Utils, ItemView,
    CustomizeView, followingItemsHtml
)-> 


    class FollowingItemsView extends CompositeView

        id: 'following_items'

        initialize: (options)->  
            @$el.html(followingItemsHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@collection, 'add', @addCallback)
            itemWidth = 235 + 15
            @positions = {
                1: {left: 165, top: 65, bottom: 65},
                2: {left: 165 + itemWidth, top: 65, bottom: 65}
            #    3: {left: 165 + itemWidth * 2, top: 65, bottom: 65},
            #     4: {left: 25 + itemWidth * 3, top: 65, bottom: 65}
            }

        events: {
            'click .following_items-fetch-more': 'fetch',
            'click .following-items-new-customize': 'newCustomize'
        }

        fetch: ->
            @collection.fetch({
                data: {page: @collection.page},
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @)
            docHeight = $(document).height() + 25
            @$('.following_items-fetch-more').css({
                'position': 'absolute',
                'left': '165px',
                'top': "#{docHeight}px",
                'z-index': 1,
                'margin-bottom': '50px'
            })

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        newCustomize: ->
            customizeView = new CustomizeView()
            @parent.appendChild(customizeView)

        addCallback: (item)->
            itemView = new ItemView({model: item})
            @appendChildTo(itemView, '.following-items')
            @collection.number++
            thumbHeight = item.get('img_meta').vdimensions.thumb[1]
            itemView.$('.item-thumb').css(
                'height', thumbHeight
            )
            lefttop = Utils.calculate_position(
                @positions,
                @collection.number,
                itemView.$el.outerHeight() + 25 
            )
            itemView.$el.css({
                'position': 'absolute',
                'z-index': 1,
                'left': "#{lefttop[0]}px",
                'top': "#{lefttop[1]}px",
                'width': '235px',
                'cursor': 'pointer',
                'border-radius': '5px',
                'background-color': 'rgb(255, 255, 255)',
                'box-shadow': '0 1px 2px 0 rgba(0,0,0,0.22)'
            })
