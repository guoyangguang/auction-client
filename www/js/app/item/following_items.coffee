define [
    'backbone',
    'cs!app/item/item'
], (Backbone, Item)->


    class FollowingItems extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @number = 0
            @following = options.following if options.following

        model: Item

        url: ->
            "/api/followings/#{@following.id}/items"
