define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'text!app/template/item/new.html'
], (
    _, CompositeView, FlashMessage, newHtml
)-> 


    class NewItemView extends CompositeView

        id: 'new-item'

        initialize: (options)->  
            @$el.html(newHtml)
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successCreate', 'errorCreate')

        events: {
            'click .new-item-sub': 'create'
        }

        create: ()->
            formData = new FormData(@$('form')[0])
            # options to tell jQuery not to process data
            options = {
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                wait: true, 
                success: @successCreate,
                error: @errorCreate
            }
            @collection.create(null, options)

        successCreate: (model, response, options)->
            @flashMsg.local('已添加.')

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)
