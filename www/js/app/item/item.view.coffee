define [
    'underscore',
    'cs!compositeview'
    'cs!app/item/item_show',
    'cs!app/item/item_show.view',
    'text!app/template/item/item.html'
], (_, CompositeView, ItemShow, ItemShowView, itemHtml)->


    class ItemView extends CompositeView

        className: 'item'

        events: {
            'click': 'show'
        }

        show: ->
            if window.App.customerRouter.ifSignin()            
                itemShow = new ItemShow(null, {itemId: @model.id})
                itemShowView = new ItemShowView({model: itemShow})
                itemShowView.fetch()
                @parent.appendChild(itemShowView)

        render: ->
            template = _.template(itemHtml)
            @$el.html(template(@model.attributes))
            @
