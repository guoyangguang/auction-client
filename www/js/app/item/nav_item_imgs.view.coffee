define [
    'underscore',
    'cs!compositeview',
    'text!app/template/item/nav_item_imgs.html',
    'text!app/template/item/itemimg.html'
], (
    _, CompositeView, navItemImgsHtml, itemImgHtml
)->

 
    class NavItemImgsView extends CompositeView

        className: 'nav-item-imgs'

        initialize: (options)->
            @item = options.item
            @itemimgs = options.itemimgs
            @currentSelected = 1 
            @total = 1 

        events: {
            'click .arrow': 'selectNext'
        }

        selectNext: ->
            @$("li#item-img-#{@currentSelected}").removeClass('show').addClass('hide')
            @currentSelected = @currentSelected + 1
            @currentSelected = 1 if @currentSelected > @total 
            @$("li#item-img-#{@currentSelected}").removeClass('hide').addClass('show')
                
        render: ->
            template = _.template(navItemImgsHtml)
            @$el.html(template(@item))
            for itemimg in @itemimgs
                @total++
                @$('.nav-item-imgs-list').append(
                    _.template(itemImgHtml)(itemimg)
                )
                @$(".nav-item-imgs-list li:nth-child(#{@total})").attr(
                    'id', "item-img-#{@total}"
                )
            @
