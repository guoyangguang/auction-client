define [
    'cs!compositeview',
    'cs!app/item/artist_item_show',
    'cs!app/item/artist_item_show.view',
    'text!app/template/item/artist_item.html'
], (CompositeView, ArtistItemShow, ArtistItemShowView, artistItemHtml)->

 
    class ArtistItemView extends CompositeView

        tagName: 'li'
        className: 'artist-item'

        events: {
            'click': 'show'
        }

        show: ->
            artistItem = new ArtistItemShow()
            artistItem.url = "/api/artists/#{@parent.collection.artist.id}/items/#{@model.id}"
            artistItemShowView = new ArtistItemShowView({model: artistItem})
            artistItemShowView.fetch()
            @parent.appendChild(artistItemShowView)

        render: ->
            template = _.template(artistItemHtml)
            @$el.html(template(@model.attributes))
            @
