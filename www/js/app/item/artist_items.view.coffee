define [
    'underscore',
    'cs!compositeview',
    'cs!app/item/artist_item.view',
    'cs!app/item/new.view',
    'cs!flashmessage',
    'cs!utils',
    'text!app/template/item/artist_items.html',
], (
    _, CompositeView, ArtistItemView, NewItemView,
    FlashMessage, Utils, artistItemsHtml
)-> 


    class ArtistItemsView extends CompositeView

        id: 'artist-items'

        initialize: (options)->  
            @$el.html(artistItemsHtml)
            # newitemView shares collection
            newItemView = new NewItemView({collection: @collection})
            @appendChildTo(newItemView, '.artist-items-popup')
            @flashMsg = new FlashMessage()
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@collection, 'add', @addCallback)


        events: {
            'click .artist-items-close': 'close',
            'click .artist-items-fetch-more': 'fetch'
        }

        close: ->
            @leave()

        fetch: ->
            data = {
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, '.artist-items')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        addCallback: (item)->
            artistItemView = new ArtistItemView({model: item})
            @appendChildTo(artistItemView, '.artist-items')
