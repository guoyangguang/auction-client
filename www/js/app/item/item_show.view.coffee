define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'cs!app/like/like_mine',
    'cs!app/like/like_mine.view',
    'cs!app/access_token/share_btn.view'
    'cs!app/item/nav_item_imgs.view',
    'cs!app/comment/item_comment',
    'cs!app/comment/item_comments',
    'cs!app/comment/item_comments.view',
    'cs!app/artist/item_artist',
    'cs!app/artist/item_artist_mini.view',
    'cs!app/bid/item_bids',
    'cs!app/bid/item_bids.view',
    'text!app/template/item/item_show.html'
], (
    _, CompositeView, FlashMessage, LikeMine, LikeMineView, ShareBtnView,
    NavItemImgsView, ItemComment, ItemComments, ItemCommentsView,
    ItemArtist, ItemArtistMiniView, ItemBids, ItemBidsView, itemShowHtml
)->

 
    class ItemShowView extends CompositeView

        id: 'item-show'

        initialize: (options)->
            @$el.html(itemShowHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @flashMsg = new FlashMessage()
            @listenTo(@model, 'change', @changeCallback)

        events: {
            'click .item-show-close': 'close',
            'click .item-show-bids-btn': 'show_bids'
        }

        close: ->
            @leave()

        show_bids: ->
            if @itemBidsView
                window.clearInterval(@itemBidsView.interval)
                @itemBidsView.leave()
                @itemBidsView = null
            else
                itemBids = new ItemBids(null, {item: @model.get('item')})
                itemBidsView = new ItemBidsView({
                    collection: itemBids,
                    item: @model.get('item')
                })
                itemBidsView.fetch()
                @itemBidsView = itemBidsView
                @appendChildTo(itemBidsView, '.item-show-header')

        fetch: ->
            @model.fetch(
                {wait: true, success: @successFetch, error: @errorFetch}
            ) 

        successFetch: (model, response, options)->

        errorFetch: (model, response, options)->
            @flashMsg.remote(response)

        changeCallback: ->
            @$('.item-show-header').prepend(
                _.template("<h3 class='ta-center'><%=name%></h3>")(@model.get('item'))
            )

            likeView = new LikeMineView({
                model: new LikeMine(),
                itemId: @model.get('item').id,
                isLiked: @model.get('is_liked'),
                likeCount: @model.get('like_count')
            })
            @appendChildTo(likeView, '.item-show-header ul')

            shareBtnView = new ShareBtnView(
                {itemId: @model.get('item').id}
            )
            @appendChildTo(shareBtnView, '.item-show-header ul')

            itemArtist = new ItemArtist(@model.get('artist'))
            itemArtistMiniView = new ItemArtistMiniView({model: itemArtist})
            @artistId = itemArtist.id
            @isFollowed = @model.get('is_followed')
            @appendChildTo(itemArtistMiniView, '.item-show-header ul')

            navItemImgsView = new NavItemImgsView({
                 item: @model.get('item'),
                 itemimgs: @model.get('itemimgs')
            })
            @appendChildTo(navItemImgsView, '.item-show-popup')

            commentList = []
            for comment in @model.get('comments')
                commentList.push(new ItemComment(comment))
            comments = new ItemComments(
                commentList, {itemId: @model.get('item').id}
            )
            commentsView = new ItemCommentsView({collection: comments}) 
            @appendChildTo(commentsView, '.item-show-popup')

            @$('.item-show-header ul').append(
                "<button type='button' class='item-show-bids-btn'>竞拍</button>"
            )
