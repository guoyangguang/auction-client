define [
    'backbone',
    'cs!app/item/item'
], (Backbone, Item)->


    class Items extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0
            @number = 0

        url: '/api/items'

        model: Item
