define [
    'underscore',
    'cs!compositeview',
    'cs!app/bid/item_bid.view',
    'cs!flashmessage',
    'cs!utils',
    'text!app/template/bid/item_bids.html',
    'text!app/template/bid/timer.html'
], (
    _, CompositeView, ItemBidView, FlashMessage, Utils,
    itemBidsHtml, timerHtml
)->

 
    class ItemBidsView extends CompositeView

        id: 'item-bids'

        initialize: (options)->
            @flashMsg = new FlashMessage()
            @$el.append(
                _.template(itemBidsHtml)(options.item)
            )
            @startingBid = new Date(options.item.starting_bid)
            @closingBid = new Date(options.item.closing_bid)
            @listenTo(@collection, 'add', @addCallback)
            _.bindAll(
                @, 'successFetch', 'errorFetch', 'successBid',
                'errorBid', 'timer'
            )
            @interval = window.setInterval(@timer, 1000)

        events: {
            'click .item-bids-fetch-more': 'fetch',
            'click .item-bids-bid': 'bid'
        }

        fetch: ->
            data = {
                page: @collection.page 
            }
            @collection.fetch({
                data: data,
                wait: true,
                remove: false,
                success: @successFetch, error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @, '.item-bids')

        errorFetch: (collection, response, options)->
            @flashMsg.remote(response)

        bid: ->
            @collection.create(
                {price: @$("input[name='item-bids-price']").val()},
                {wait: true, success: @successBid, error: @errorBid}
            )

        successBid: (model, response, options)->
            @$('.item-bids-price').val('')
            @flashMsg.local('已出价.')
            if model.get('status') == 2
                bids = @collection.where({status: 2})
                for bid in bids
                    bid.set({status: 1}) unless bid.id == model.id

        errorBid: (model, response, options)->
            @flashMsg.remote(response)

        addCallback: (bid)->
            itemBidView = new ItemBidView({model: bid})
            @appendChildTo(itemBidView, '.item-bids')
            console.log @collection.length

        timer: ->
            now = new Date()
            if now.getTime() < @startingBid.getTime()
                window.clearInterval(@interval)
                return @$('.item-bids-timer').html(
                    '仍未开拍, 欢迎您届时前来竞拍.'
                )
            if now.getTime() >= @closingBid.getTime()
                window.clearInterval(@interval)
                return @$('.item-bids-timer').html('拍卖已结束.')
            leftTime = @closingBid.getTime() - now.getTime()
            leftSeconds = parseInt(leftTime/1000)
            days = Math.floor(leftSeconds/(24*60*60))
            hours = Math.floor((leftSeconds-days*24*60*60)/3600)
            minutes = Math.floor((leftSeconds-days*24*60*60-hours*3600)/60)
            seconds = Math.floor(
                leftSeconds-days*24*60*60-hours*3600-minutes*60
            )
            return @$('.item-bids-timer').html(_.template(timerHtml)(
                {days: days, hours: hours, minutes: minutes, seconds: seconds}
            ))
