define [
    'cs!compositeview',
    'text!app/template/bid/item_bid.html'
], (
    CompositeView, itembidHtml
)->

 
    class ItemBidView extends CompositeView

        className: 'item-bid'
        tagName: 'li'

        initialize: (options)->
            @listenTo(@model, 'change:status', @updateViewStatus)

        updateViewStatus: ->
            @$('.item-bid-status').text(@model.get('status'))

        render: ->
            @$el.html(
                _.template(itembidHtml)(@model.attributes)
            )
            if @model.get('status') == 3
                @$('.item-bid-status-and').append(            
                    "<span><button type='button' class='item-bid-pay'>支付</button></span>"
                )
            @
          
