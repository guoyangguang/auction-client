define [
    'backbone',
    'cs!app/bid/item_bid'
], (Backbone, ItemBid)->


    class ItemBids extends Backbone.Collection
 
        initialize: (models, options)-> 
            @item = options.item
            @page = 1
            @currentLen = 0

        model: ItemBid

        url: -> "/api/items/#{@item.id}/bids"
