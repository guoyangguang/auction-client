define [
  'backbone',
  'cs!app/followship/my_following'
], (Backbone, MyFollowing)->


    class MyFollowings extends Backbone.Collection

        model: MyFollowing

        url: '/api/followings/mine' 
