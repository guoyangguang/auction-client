define [
  'backbone',
  'cs!app/followship/artist_followed'
], (Backbone, ArtistFollowed)->


    class ArtistFolloweds extends Backbone.Collection
        # artist followeds have not developed yet.
        initialize: (models, options)->
            @page = 1

        model: ArtistFollowed

        url: '/api/artist/followeds'
             
