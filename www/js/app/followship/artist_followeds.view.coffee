define [
  'underscore',
  'cs!compositeview',
  'cs!app/followship/artist_followed.view',
  'cs!utils',
  'cs!flashmessage',
  'text!app/template/followship/artist_followeds.html'
], (
    _, CompositeView, ArtistFollowedView, Utils, FlashMessage,
    artistFollowdesHtml
)->


    class ArtistFollowedsView extends CompositeView

        id: 'artist-followeds'

        initialize: (options)->
            @$el.html(artistFollowdesHtml)
            @flashMessage = new FlashMessage()
            @listenTo(@collection, 'add', @addCallback)
            _.bindAll(@, 'successFetch', 'errorFetch')

        events: {
            'click .artist-followeds-fetch-more': 'fetch'
        }

        fetch: ->
            @collection.fetch({
                data: {page: @collection.page},
                wait: true,
                remove: false,
                success: @successFetch,
                error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @)

        errorFetch: (collection, response, options)->
            @flashMessage.remote(response)

        addCallback: (followed)->
            artistFollowedView = new ArtistFollowedView({model: followed})
            @prependChild(artistFollowedView)
