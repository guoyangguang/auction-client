define [
  'underscore',
  'cs!compositeview',
  'cs!app/followship/my_following.view',
  'cs!flashmessage',
  'text!app/template/followship/my_followings.html'
], (
    _, CompositeView, MyFollowingView, FlashMessage, myFollowingsHtml
)->


    class MyFollowingsView extends CompositeView

        id: 'my-followings'

        initialize: (options)->
            @$el.html(myFollowingsHtml)
            @flashMessage = new FlashMessage()
            @listenTo(@collection, 'add', @addCallback)
            _.bindAll(@, 'successFetch', 'errorFetch')

        events: {
        }

        fetch: ->
            @collection.fetch({
                wait: true,
                remove: false,
                success: @successFetch,
                error: @errorFetch
            })

        successFetch: (collection, response, options)->
            if collection.length == 0
                @flashMessage.local(
                    '您的关注数量是0，请关注您喜爱的书法老师!'
                )

        errorFetch: (collection, response, options)->
            @flashMessage.remote(response)

        addCallback: (following)->
            myFollowingView = new MyFollowingView({model: following})
            @appendChildTo(myFollowingView, '.my-followings')
