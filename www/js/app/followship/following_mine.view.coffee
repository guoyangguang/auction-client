define [
    'underscore',
    'cs!compositeview',
    'cs!flashmessage',
    'text!app/template/followship/following_mine.html'
], (_, CompositeView, FlashMesssage, followingMineHtml)->


    class FollowingMineView extends CompositeView

        className: 'following-mine'

        initialize: (options)->
            @$el.html(followingMineHtml)
            _.bindAll(
                @, 'successFollow', 'errorFollow',
                'successUnFollow', 'errorUnFollow' 
            )
            @flashMsg = new FlashMesssage()

        events: {
            'click button': 'followUnfollow'
        }

        followUnfollow: ->
            if @parent.parent.isFollowed
                @unfollow()
            else
                @follow()

        follow: ->
            @model.url = '/api/followings/mine'
            data = {
                artist_id: @parent.parent.artistId
            }
            options = {
                type: 'POST',
                wait: true,
                success: @successFollow,
                error: @errorFollow
            }
            @model.save(
                data,
                options
            )

        successFollow: (model, response, options)->
            @$('button').text('取消关注')
            @parent.parent.isFollowed = true
            @flashMsg.local('已关注.')

        errorFollow: (model, response, options)->
            @flashMsg.remote(response)

        unfollow: ->
            @model.url = "/api/followings/#{@parent.parent.artistId}/mine"
            options = {
                type: 'PUT',
                wait: true,
                success: @successUnFollow,
                error: @errorUnFollow
            }
            @model.save(
                null,
                options
            )

        successUnFollow: (model, response, options)->
            @$('button').text('关注')
            @parent.parent.isFollowed = false 
            @flashMsg.local('已取消关注.')

        errorUnFollow: (model, response, options)->
            @flashMsg.remote(response)

        render: ->
            if @parent.parent.isFollowed
                @$('button').text('取消关注')
            else
                @$('button').text('关注')
