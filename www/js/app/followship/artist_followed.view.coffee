define [
  'underscore',
  'cs!compositeview',
  'cs!flashmessage',
  'cs!app/template/share/appdata',
  'text!app/template/followship/artist_followed.html'
], (
    _, CompositeView, FlashMessage, AppData, artistFollowedHtml
)->


    class ArtistFollowedView extends CompositeView

        className: 'artist-followed'

        initialize: (options)->

        events: {

        }

        render: ->
            template = _.template(artistFollowedHtml)
            @$el.html(template(@model.attributes))
            gender = AppData.gender[@model.get('gender')]
            job = AppData.job[@model.get('job')]
            @$('.artist-followed-gender').text(gender)
            @$('.artist-followed-job').text(job)
            @
