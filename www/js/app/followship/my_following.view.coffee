define [
  'cs!compositeview',
  'cs!app/item/following_items',
  'cs!app/item/following_items.view',
  'text!app/template/followship/my_following.html'
], (
    CompositeView, FollowingItems, FollowingItemsView,
    myFollowingHtml
)->


    class MyFollowingView extends CompositeView

        className: 'my-following'
        tagName: 'li'

        initialize: (options)->

        events: {
            'click .my-following-photo': 'followingItems'
        }

        followingItems: ->
            if window.App.customerRouter.ifSignin()
                followingItems = new FollowingItems(
                    null, {following: @model}
                )
                followingItemsView = new FollowingItemsView(
                    {collection: followingItems}
                )
                followingItemsView.fetch()
                @parent.parent.swap(followingItemsView)
                @parent.parent.appendChild(followingItemsView)

        render: ->
            template = _.template(myFollowingHtml)
            @$el.html(template(@model.attributes))
            @
