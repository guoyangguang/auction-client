define [
    'cs!compositeview',
    'underscore',
    'cookies',
    'cs!lib/count_unread_msgs_ws',
    'cs!flashmessage',
    'text!app/template/user/signin.html'
], (
    CompositeView, _, Cookies,
    CountUnreadMsgsWs, FlashMessage, signinHtml
)->
  
    class SigninView extends CompositeView
    
        id: 'signin'

        initialize: (options)->
            @$el.html(signinHtml)
            _.bindAll(
                @, 'errorCreate', 'successCreate'
            )
            @flashMessage = new FlashMessage()
            @listenTo(@model, 'change', @changeCallback)

        events: {
            'click .signin-submit': 'signin',
            'click .signin-signup': 'navSignup',
            'click .signin-forget-password': 'navForgetPassword'
        }

        signin: ->
            @model.url = '/api/signin'
            signinData = {
                phone: @$('input[name="phone"]').val(),
                password: @$('input[name="password"]').val(),
                captcha_data: @$('input[name="captcha"]').val()
            }
            @model.save(
                signinData,
                {
                    wait: true,
                    success: @successCreate,
                    error: @errorCreate
                }
            )
        
        successCreate: (model, response, options)->

        errorCreate: (model, response, options)->
            @flashMessage.remote(response)

        changeCallback: ->
            Cookies.set('logged_in', 'yes')
            Cookies.set(
                'profile_avatar_url', @model.get('profile')['thumb_file']
            )
            @model.clear {silent: true}
            # window.App.countUnreadMsgsWs = new CountUnreadMsgsWs(
            #     window.localStorage.getItem('accessToken')
            # )
            window.App.customerRouter.customerMenuView.appendMeMenuView()
            window.App.customerRouter.navigate('', {trigger: true})

        navSignup: ->
            window.App.customerRouter.navigate('signup', {trigger: true})

        navForgetPassword: ->
            window.App.customerRouter.navigate('reset_password', {trigger: true})
