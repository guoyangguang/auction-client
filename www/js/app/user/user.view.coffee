define [
    'underscore',
    'cs!compositeview',
    'cs!app/user/show.view',
    'cs!app/assignment/assignment',
    'cs!app/assignment/assignment.view',
    'text!app/template/user/user.html'
], (_, CompositeView, ShowUserView, Assignment, AssignmentView, userHtml)->


    class UserView extends CompositeView

        className: 'user'
        tagName: 'li'

        events: {
            'click .user-show': 'showUser',
            'click .user-assign-role': 'assignRole'
        }

        showUser: ->
            showUserView = new ShowUserView({model: @model})
            @appendChild(showUserView)

        assignRole: ->
            @currentAssignView.leave() if @currentAssignView
            assignment = new Assignment(
                {'user_id': @model.get('id')}
            )
            assignmentView = new AssignmentView({model: assignment})
            @currentAssignView = assignmentView
            @appendChild(assignmentView)

        render: ->
            template = _.template(userHtml)
            @$el.html(template(@model.attributes))
            @
