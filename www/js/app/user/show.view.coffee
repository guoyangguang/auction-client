define [
    'underscore',
    'cs!compositeview',
    'text!app/template/user/show.html'
], (_, CompositeView, showHtml)->


    class ShowUserView extends CompositeView

        className: 'showuser'

        events: {
            'click .showuser-close': 'close'
        }

        close: ->
            @leave()

        render: ->
            template = _.template(showHtml)
            @$el.html(template(@model.attributes))
            @
