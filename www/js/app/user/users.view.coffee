define [
    'underscore',
    'cs!compositeview',
    'cs!app/user/user.view',
    'cs!flashmessage',
    'cs!utils',
    'text!app/template/user/users.html'
], (_, CompositeView, UserView, FlashMessage, Utils, usersHtml)->


    class UsersView extends CompositeView

        id: 'users'

        initialize: (options)->
            @$el.html(usersHtml)
            _.bindAll(@, 'successFetch', 'errorFetch')
            @listenTo(@collection, 'add', @addCallback)
            @flashMsg = new FlashMessage()

        events: {
            'click .users-fetch-more': 'fetch'
        } 

        fetch: ->
            @collection.fetch({
                data: {page: @collection.page},
                wait: true,
                remove: false,
                success: @successFetch,
                error: @errorFetch
            })

        successFetch: (collection, response, options)->
            Utils.ifFetchMore(collection, @)

        errorFetch: (collection, response, options)-> 
            @flashMsg.remote(response)

        addCallback: (user)->
            userView = new UserView({model: user})
            @appendChildTo(userView, '.users')
