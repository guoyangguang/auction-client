define [
    'cs!compositeview',
    'underscore',
    'cs!app/verification/verification',
    'cs!flashmessage',
    'text!app/template/user/reset_password.html'
], (CompositeView, _, Verification, FlashMessage, resetPasswordHtml)->
  
    class ResetPasswordView extends CompositeView
    
        id: 'reset-password'

        initialize: (options)->
            @$el.html(resetPasswordHtml)
            _.bindAll(
                @, 'successReset', 'errorReset',
                'successVerif', 'errorVerif', 'timer' 
            )
            @flashMessage = new FlashMessage()
            @startTime = 60
            @phoneRe = new RegExp("^1[0-9]{10}$")

        events: {
            'click .reset-password-submit': 'resetPassword', 
            'click .reset-password-send-verif': 'sendVerificationCode' 
        }

        resetPassword: ->
            @model.url = '/api/reset_password'
            data = {
                phone: @$('input[name="phone"]').val(),
                verification_code: @$('input[name="verification_code"]').val(),
                password: @$('input[name="password"]').val(), 
                password_confirmation: @$('input[name="password_confirmation"]').val(), 
                captcha_data: @$('input[name="captcha"]').val()
            }
            @model.save(
                data,
                {wait: true, success: @successReset, error: @errorReset} 
            )

        successReset: (model, response, options)->
            @flashMessage.local('请用新密码登录.') 
            window.App.customerRouter.navigate('signin', {trigger: true})

        errorReset: (model, response, options)->
            @flashMessage.remote(response) 

        sendVerificationCode: ->
            phone = @$('input[name="phone"]').val()
            if @phoneRe.exec(phone)
                verification = new Verification()
                verification.save(
                    {phone: phone},
                    {
                        wait: true, 
                        success: @successVerif,
                        error: @errorVerif
                    }
                )
                @$('.reset-password-send-verif').replaceWith(
                    '<button class="timer"><span class="timer-sec">60</span>秒后重新获取</button>'
                )
                @interval = window.setInterval(@timer, 1000)
            else
                @flashMessage.local('请输入手机号码.')

        successVerif: (model, response, options)->

        errorVerif: (model, response, options)->
            @flashMessage.remote(response)

        timer: ->
            @startTime = @startTime - 1
            if @startTime <= 0
                clearInterval(@interval)
                @$('.timer').replaceWith(
                    "<button id='reset-password-send-verif' class='reset-password-send-verif' type='button'>获取验证码</button>"

                )
                @startTime = 60
                return @startTime
            @$('.timer-sec').html(@startTime)
