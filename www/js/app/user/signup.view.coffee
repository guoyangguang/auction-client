define [
    'cs!compositeview',
    'cs!app/verification/verification',
    'cs!flashmessage',
    'text!app/template/user/signup.html'
], (CompositeView, Verification, FlashMessage, signupHtml)->
  
    class SignupView extends CompositeView
    
        id: 'signup'

        initialize: (options)->
            @$el.html signupHtml 
            _.bindAll(
                @, 'successSignup', 'errorSignup',
                'successVerif', 'errorVerif', 'timer'
            )
            @flashMessage = new FlashMessage()
            @startTime = 60
            @phoneRe = new RegExp("^1[0-9]{10}$")

        events: {
            'click .signup-submit': 'signup',
            'click .signup-send-verif': 'sendVerificationCode'
        }

        signup: ->
            if @$("input[name='accept']:checked").val()
                @model.url = '/api/signup' 
                signupData = {
                    phone: @$('input[name="phone"]').val(),
                    verification_code: @$('input[name="verification_code"]').val(),
                    email: @$('input[name="email"]').val(),
                    password: @$('input[name="password"]').val(),
                    password_confirmation: @$('input[name="password_confirmation"]').val(),
                    captcha_data: @$('input[name="captcha"]').val(),
                    accept: @$("input[name='accept']:checked").val()
                }
                @model.save(
                    signupData,
                    {
                        wait: true, 
                        success: @successSignup,
                        error: @errorSignup
                    }
                ) 
            else
                @flashMessage.local('请阅读并接受协议条款.')
        
        successSignup: (model, response, options)->
            window.App.customerRouter.navigate('signin', {trigger: true})
        
        errorSignup: (model, response, options)->
            @flashMessage.remote(response)

        sendVerificationCode: ->
            phone = @$('input[name="phone"]').val()
            if @phoneRe.exec(phone)
                verification = new Verification()
                verification.save(
                    {phone: phone},
                    {
                        wait: true, 
                        success: @successVerif,
                        error: @errorVerif
                    }
                )
                @$('.signup-send-verif').replaceWith(
                    '<button class="timer"><span class="timer-sec">60</span>秒后重新获取</button>'
                )
                @interval = window.setInterval(@timer, 1000)
            else
                @flashMessage.local('请输入手机号码.')

        successVerif: (model, response, options)->

        errorVerif: (model, response, options)->
            @flashMessage.remote(response)

        timer: ->
        # timer let user wait some time to launch next request
            @startTime = @startTime - 1
            if @startTime <= 0
                window.clearInterval(@interval)
                @$('.timer').replaceWith(
                    "<button id='signup-send-verif' class='signup-send-verif' type='button'>获取验证码</button>"
                )
                @startTime = 60
                return @startTime
            @$('.timer-sec').html(@startTime)
