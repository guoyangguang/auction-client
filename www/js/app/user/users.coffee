define ['backbone', 'cs!app/user/user'], (Backbone, User)-> 


    class Users extends Backbone.Collection

        initialize: (models, options)->
            @page = 1
            @currentLen = 0

        model: User

        url: '/api/users'
