define [
    'cs!compositeview', 
    'underscore',
    'cookies',
    'cs!flashmessage'
], (CompositeView, _, Cookies, FlashMessage)->

    class SignoutView extends CompositeView

        initialize: (options)->
            _.bindAll(
                @, 'successSignout', 'errorSignout'
            )
            @flashMessage = new FlashMessage()

        signOut: ->
            @model.url = '/api/signout'
            @model.save(
                {},
                {wait: true, success: @successSignout, error: @errorSignout}
            )

        successSignout: (model, response, options)->
            # window.App.countUnreadMsgsWs.close()
            window.App.customerRouter.customerMenuView.customerMeMenuView.leave()
            Cookies.remove('logged_in')
            Cookies.remove('profile_avatar_url')
            window.App.customerRouter.navigate('signin', {trigger: true})
        
        errorSignout: (model, response, options)->
            @flashMessage.remote(response)
