define [
    'underscore',
    'cs!compositeview',
    'text!app/template/role/role.html'
], (_, CompositeView, roleHtml)->


    class RoleView extends CompositeView

        className: 'role'
        tagName: 'li'

        render: ->
            template = _.template(roleHtml)
            @$el.html(template(@model.attributes))
            @
