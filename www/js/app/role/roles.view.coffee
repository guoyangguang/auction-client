define [
    'underscore',
    'cs!compositeview',
    'cs!app/role/role.view',
    'cs!flashmessage',
    'text!app/template/role/roles.html'
], (_, CompositeView, RoleView, FlashMessage, rolesHtml)->


    class RolesView extends CompositeView

        id: 'roles'

        initialize: (options)->
            @$el.html(rolesHtml)
            _.bindAll(
                @, 'successFetch', 'errorFetch',
                'successCreate', 'errorCreate'
            )
            @listenTo(@collection, 'add', @addCallback)
            @flashMsg = new FlashMessage()

        events: {
            'click .roles-form button': 'create'
        } 

        create: ->
            @collection.create(
                {'name': @$('.roles-form input').val()},
                {
                    wait: true,
                    success: @successCreate,
                    error: @errorCreate
                }
            )

        successCreate: (model, response, options)->

        errorCreate: (model, response, options)->
            @flashMsg.remote(response)

        fetch: ->
            @collection.fetch({
                wait: true,
                remove: false,
                success: @successFetch,
                error: @errorFetch
            })

        successFetch: (collection, response, options)->

        errorFetch: (collection, response, options)-> 
            @flashMsg.remote(response)

        addCallback: (role)->
            roleView = new RoleView({model: role})
            @appendChildTo(roleView, '.roles')
