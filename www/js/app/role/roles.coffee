define ['backbone', 'cs!app/role/role'], (Backbone, Role)-> 


    class Roles extends Backbone.Collection

        model: Role 

        url: '/api/roles'
